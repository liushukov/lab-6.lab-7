### Dealer app on Spring Boot

![Скріншот1](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/1.png?ref_type=heads)
**Non authenticated greeting page**
---
![Скріншот2](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/2.png?ref_type=heads)
**Registration**
---
![Скріншот3](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/3.png?ref_type=heads)
**Users table**
---
![Скріншот4](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/4.png?ref_type=heads)
**Validation for authentication**
---
![Скріншот5](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/5.png?ref_type=heads)
**Authenticated greeting page, Role - Admin**
---
![Скріншот6](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/6.png?ref_type=heads)
**Authenticated greeting page, Role - User**
---
![Скріншот7](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/7.png?ref_type=heads)
**Page with cars**
---
![Скріншот8](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/8.png?ref_type=heads)
**Page with cars, sorting by brand**
---
![Скріншот9](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/9.png?ref_type=heads)
**More details about the car**
---
![Скріншот10](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/10.png?ref_type=heads)
**View photo of the car**
---
![Скріншот11](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/11.png?ref_type=heads)
**Edit brands, Role - Admin**
---
![Скріншот12](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/12.png?ref_type=heads)
**Adding new brand, Role - Admin**
---
![Скріншот13](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/13.png?ref_type=heads)
**Updating brand's name, Role - Admin**
---
![Скріншот14](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/14.png?ref_type=heads)
**Added new brand 'Porsche' and updated 'Mercedes', Role - Admin**
---
![Скріншот15](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/15.png?ref_type=heads)
![Скріншот16](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/16.png?ref_type=heads)
![Скріншот17](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/17.png?ref_type=heads)
**Adding new car to brand, Role - Admin**
---
![Скріншот18](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/18.png?ref_type=heads)
**Page with editing cars in brand, Role - Admin**
---
![Скріншот19](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/19.png?ref_type=heads)
**Updating info for car, Role - Admin**
---
![Скріншот20](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/20.png?ref_type=heads)
**Updated car's info, Role - Admin**
---
![Скріншот21](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/21.png?ref_type=heads)
**Deleted car in brand, Role - Admin**
---
![Скріншот22](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/22.png?ref_type=heads)
**Deleted brand, Role - Admin**
---
![Скріншот23](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/23.png?ref_type=heads)
**Table with brands**
---
![Скріншот24](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/24.png?ref_type=heads)
**Table with photos**
---
![Скріншот25](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/doc1.jpeg?ref_type=heads)
**how session works**
---
![Скріншот26](https://gitlab.com/liushukov/lab-6.lab-7/-/raw/main/images/readme/doc2.png?ref_type=heads)
**authentication**
---